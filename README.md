
# Cascading Wallpaper Utility

![Screenshot](./Info/screenshot2.png)

A simple cross-platform program (currently Windows™ only) to change the desktop wallpaper periodically, optionally depending on date and time.

Source code licensed under MS-PL. All image assets are Copyright © 2020 Kameko.
