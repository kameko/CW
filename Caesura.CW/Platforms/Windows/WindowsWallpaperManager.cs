
namespace Caesura.CW.Platforms.Windows
{
    using System;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Drawing;
    using System.Drawing.Imaging;
    using Microsoft.Win32;
    
    public class WindowsWallpaperManager : IWallpaperManagerAbstraction
    {
        public DirectoryInfo TempFolder { get; private set; }
        
        public WindowsWallpaperManager()
        {
            TempFolder = null!;
        }
        
        public void RunOnce()
        {
            try
            {
                SetTempDirectory(Path.GetTempPath());
                EnsureDisableTile();
            }
            catch
            {
                // Nothing.
            }
        }
        
        public void SetTempDirectory(string path) => SetTempDirectory(new DirectoryInfo(path));
        
        public void SetTempDirectory(DirectoryInfo dir)
        {
            if (!dir.Exists)
            {
                dir.Create();
                
                if (!dir.Exists)
                {
                    throw new IOException($"\"{dir.FullName}\" cannot be created.");
                }
            }
            
            TempFolder = dir;
        }
        
        public bool SetWallpaper(Stream image_stream, WallpaperStyle style)
        {
            try
            {
                var temp_path = Path.Combine(TempFolder.FullName, "cw_wall.bmp");
                SetRegistrySetting(image_stream, style, temp_path);
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public void Dispose()
        {
            // Nothing.
        }
        
        // --- PRIVATE --- //
        
        private static void SetRegistrySetting(Stream image_stream, WallpaperStyle style, string temp_path)
        {
            var image = Image.FromStream(image_stream);
            image.Save(temp_path, ImageFormat.Bmp);
            
            var key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
            
            if (key is null)
            {
                throw new NullReferenceException(nameof(key));
            }
            
            key.SetValue("WallpaperStyle", ((int)style).ToString());
            
            SetParameters(temp_path);
        }
        
        private static void EnsureDisableTile()
        {
            var key = Registry.CurrentUser.OpenSubKey(@"Control Panel\Desktop", true);
            
            if (key is null)
            {
                throw new NullReferenceException(nameof(key));
            }
            
            key.DeleteValue("TileWallpaper", false);
        }
        
        // --- P/INVOKES --- //
        
        private const int SPI_SETDESKWALLPAPER = 20;
        private const int SPIF_UPDATEINIFILE = 0x01;
        private const int SPIF_SENDWININICHANGE = 0x02;
        
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        private static extern int SystemParametersInfo(int uAction, int uParam, string lpvParam, int fuWinIni);
        
        private static void SetParameters(string temp_path)
        {
            SystemParametersInfo(
                SPI_SETDESKWALLPAPER,
                0,
                temp_path,
                SPIF_UPDATEINIFILE | SPIF_SENDWININICHANGE
            );
        }
    }
}
