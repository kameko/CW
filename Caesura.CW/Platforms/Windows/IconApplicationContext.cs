
namespace Caesura.CW.Platforms.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Reflection;
    using System.Windows.Forms;
    using System.Drawing;
    using Microsoft.Extensions.FileProviders;
    
    public class IconApplicationContext : ApplicationContext
    {
        private WindowsPlatformProvider platform;
        private NotifyIcon tray_icon;
        private bool run_at_startup_toggle;
        
        public IconApplicationContext(WindowsPlatformProvider win_platform)
        {
            platform = win_platform;
            run_at_startup_toggle = platform.IsRunningAtStartup();
            tray_icon = new NotifyIcon();
            tray_icon.Icon = GetTrayIcon();
            tray_icon.Text = "Cascading Wallpaper Utility";
            SetupContextMenuStrip();
            tray_icon.MouseUp += OnLeftClickUp;
            tray_icon.Visible = true;
        }
        
        // --- DESIGNER --- //
        
        void SetupContextMenuStrip()
        {
            var cms = new ContextMenuStrip();
            tray_icon.ContextMenuStrip = cms;
            cms.Items.AddRange(GetToolStrips());
            // cms.Renderer = new CustomToolStripRenderer();
            // note: this is ugly when highlighting, but we can
            // change that with MouseEnter and MouseLeave events.
            // cms.BackColor = Color.DarkSlateGray;
            // cms.ForeColor = Color.WhiteSmoke;
            
            cms.Opened += (o, e) => UpdateStartupToggle(platform.IsRunningAtStartup());
            
            SetupDisableAutoClose("Next");
            SetupDisableAutoClose("Previous");
            SetupDisableAutoClose("Run at Startup");
        }
        
        ToolStripItem[] GetToolStrips() => new ToolStripItem[]
        {
            new ToolStripMenuItem("Caesura Cascading Wallpaper Utility", null, RunInfoBox, "InfoBox")
            {
                Image = GetEmbeddedImage("caesura_small.png"),
                // ForeColor = Color.Gray,
            },
            new ToolStripSeparator(),
            new ToolStripMenuItem("Open file location", null, null, "File Location")
            {
                Image = GetEmbeddedImage("explorer_button.png"),
                ToolTipText = "Open the location of the current wallpaper in Explorer.",
                ForeColor = Color.Gray,
            },
            new ToolStripMenuItem("Open config", null, null, "Open Config")
            {
                Image = GetEmbeddedImage("config_button.png"),
                ToolTipText = "Open the configuration file."
            },
            new ToolStripMenuItem("Next", null, null, "Next")
            {
                // TODO: show a subtle animation for previous and next buttons.
                Image = GetEmbeddedImage("next_button.png"),
                ToolTipText = "Set wallpaper to the next wallpaper in queue.",
                ForeColor = Color.Gray,
            },
            new ToolStripMenuItem("Previous", null, null, "Previous")
            {
                Image = GetEmbeddedImage("prev_button.png"),
                ToolTipText = "Set wallpaper to the previous wallpaper.",
                ForeColor = Color.Gray,
            },
            new ToolStripMenuItem("Start with Windows", null, RunAtStartupToggle, "Run at Startup")
            {
                Image = GetEmbeddedImage("startup_button.png"),
                ToolTipText = "Run the utility when Windows starts. (shortcut placed in Startup folder)",
                CheckState = CheckState.Unchecked,
            },
            new ToolStripMenuItem("Exit", null, Exit, "Exit")
            {
                Image = GetEmbeddedImage("exit_button.png"),
                ToolTipText = "Exit Cascading Wallpaper Utility."
            },
        };
        
        // --- CALLBACKS --- //
        
        void RunInfoBox(object? sender, EventArgs e)
        {
            // TODO: make a proper form.
            MessageBox.Show(
                "Caesura Cascading Wallpaper Utility, by Kameko. " +
                "https://gitlab.com/kameko/CW",
                "About",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information
            );
        }
        
        void RunAtStartupToggle(object? sender, EventArgs e)
        {
            UpdateStartupToggle(!run_at_startup_toggle);
        }
        
        void Exit(object? sender, EventArgs e)
        {
            // Hide tray icon, otherwise it will remain shown until user mouses over it.
            tray_icon.Visible = false;
            Application.Exit();
        }
        
        void OnLeftClickUp(object? sender, MouseEventArgs e)
        {
            // Oh, sorry, did you think the following line would work?
            // `tray_icon.ContextMenuStrip.Show(e.Location);`
            // Well it doesn't! Not unless you enjoy having your context
            // menu at location 0,0! I have no idea why MouseEventArgs's
            // position is at 0,0, but because of that, I have to use this
            // ugly reflection hack.
            // See for explanation: https://stackoverflow.com/a/2208910/12860405 
            
            ShowContextMenu();
        }
        
        // --- PRIVATE --- //
        
        void ShowContextMenu()
        {
            var scm_mi = typeof(NotifyIcon).GetMethod(
                "ShowContextMenu",
                BindingFlags.Instance | BindingFlags.NonPublic
            )!;
            scm_mi.Invoke(tray_icon, null);
        }
        
        void UpdateStartupToggle(bool on = false)
        {
            run_at_startup_toggle = on;
            var ts = (ToolStripMenuItem)tray_icon.ContextMenuStrip.Items.Find("Run at Startup", false)[0];
            ts.CheckState = run_at_startup_toggle ? CheckState.Checked : CheckState.Unchecked;
            platform.RunAtStartup(run_at_startup_toggle);
        }
        
        ToolStripMenuItem SetupDisableAutoClose(string name)
        {
            var ts = (ToolStripMenuItem)tray_icon.ContextMenuStrip.Items.Find(name, false)[0];
            ts.MouseEnter += (o, e) => tray_icon.ContextMenuStrip.AutoClose = false;
            ts.MouseLeave += (o, e) => tray_icon.ContextMenuStrip.AutoClose = true;
            return ts;
        }
        
        Icon GetTrayIcon()
        {
            var embeddedProvider = new EmbeddedFileProvider(Assembly.GetExecutingAssembly());
            
            Icon? ico = null;
            using (var reader = embeddedProvider.GetFileInfo("tray_icon.ico").CreateReadStream())
            {
                ico = new Icon(reader);
            }
            
            return ico;
        }
        
        Image GetEmbeddedImage(string path)
        {
            var embeddedProvider = new EmbeddedFileProvider(Assembly.GetExecutingAssembly());
            
            Image? caesura_img = null;
            using (var reader = embeddedProvider.GetFileInfo(path).CreateReadStream())
            {
                caesura_img = Image.FromStream(reader);
            }
            
            return caesura_img;
        }
    }
}
