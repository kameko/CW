
namespace Caesura.CW.Platforms.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows.Forms;
    using System.Drawing;
    
    public class CustomToolStripRenderer : ToolStripRenderer
    {
        public CustomToolStripRenderer()
        {
            
        }
        
        protected override void OnRenderButtonBackground(ToolStripItemRenderEventArgs e)
        {
            // e.ToolStrip.BackColor = Color.DarkSlateGray;
            // e.Item.BackColor = Color.DarkSlateGray;
            e.Graphics.FillRectangle(Brushes.DarkSlateGray, 0, 0, e.Graphics.DpiX, e.Graphics.DpiY);
            base.OnRenderButtonBackground(e);
        }
        
        protected override void OnRenderDropDownButtonBackground(ToolStripItemRenderEventArgs e)
        {
            e.Graphics.FillRectangle(Brushes.DarkSlateGray, 0, 0, e.Graphics.DpiX, e.Graphics.DpiY);
            base.OnRenderDropDownButtonBackground(e);
        }
    }
}
