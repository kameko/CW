
namespace Caesura.CW.Platforms.Windows
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.InteropServices;
    using System.Windows.Forms;
    
    public class WindowsPlatformProvider : IPlatformProvider
    {
        public OSPlatform Platform => OSPlatform.Windows;
        public IWallpaperManagerAbstraction WallpaperManager { get; private set; }
        public DirectoryInfo DataPath { get; private set; }
        
        public WindowsPlatformProvider()
        {
            WallpaperManager = new WindowsWallpaperManager();
            DataPath = new DirectoryInfo(
                Path.Combine(
                    Environment.GetFolderPath(
                        Environment.SpecialFolder.ApplicationData,
                        Environment.SpecialFolderOption.Create
                    ),
                    "Caesura.CW"
                )
            );
        }
        
        public void OnStartup()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new IconApplicationContext(this));
        }
        
        public void OnShutdown()
        {
            // Nothing.
        }
        
        public bool ChangeDataPath(DirectoryInfo dir)
        {
            if (dir.Exists)
            {
                DataPath = dir;
                return true;
            }
            return false;
        }
        
        public bool ChangeDataPath(string path) => ChangeDataPath(new DirectoryInfo(path));
        
        public bool IsValidPlatform() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
        
        public void RunAtStartup(bool run_at_startup = false)
        {
            if (run_at_startup)
            {
                // TODO: generate a PowerShell script and place it
                // in the Startup folder.
                // To make validation in IsRunningAtStartup() easier,
                // put the current path in a variable in the script
                // instead of a literal argument.
                // To make things a little more professional, we'll make
                // a template PS script as an embedded file, and replace
                // some token like <$CURRENT_EXECUTABLE_PATH>.
            }
            else
            {
                // delete the PS script.
            }
        }
        
        public bool IsRunningAtStartup()
        {
            // TODO: check if the PS script exists, if so,
            // validate it by checking if it is properly pointing to the
            // application's current directory.
            // When validating, be sure to ignore anything after the path,
            // e.g. command line arguments.
            return false;
        }
            
        public void Dispose()
        {
            // Nothing.
        }
    }
}
