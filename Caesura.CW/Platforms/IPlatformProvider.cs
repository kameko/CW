
namespace Caesura.CW.Platforms
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.InteropServices;
    
    public interface IPlatformProvider : IDisposable
    {
        OSPlatform Platform { get; }
        IWallpaperManagerAbstraction WallpaperManager { get; }
        DirectoryInfo DataPath { get; }
        
        void OnStartup();
        void OnShutdown();
        bool ChangeDataPath(DirectoryInfo dir);
        bool ChangeDataPath(string path);
        bool IsValidPlatform();
        void RunAtStartup(bool run_at_startup = false);
        bool IsRunningAtStartup();
    }
}
