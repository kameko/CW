
namespace Caesura.CW.Platforms
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface IPlatformAbstractionLayer
         : IPlatformProvider
         , IWallpaperManagerAbstraction
         , IDisposable
    {
        
    }
}
