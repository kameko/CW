
namespace Caesura.CW.Platforms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.InteropServices;
    
    public class UnsupportedPlatformProvider : IPlatformProvider
    {
        public OSPlatform Platform => OSPlatform.Create("Unsupported");
        public IWallpaperManagerAbstraction WallpaperManager => null!;
        public DirectoryInfo DataPath => null!;
        
        public UnsupportedPlatformProvider()
        {
            
        }
        
        public void OnStartup() { }
        
        public void OnShutdown() { }
        
        public bool ChangeDataPath(DirectoryInfo dir) => false;
        
        public bool ChangeDataPath(string path) => false;
        
        public bool IsValidPlatform() => false;
        
        public void RunAtStartup(bool run_at_startup = false) { }
        
        public bool IsRunningAtStartup() => false;
        
        public void Dispose()
        {
            // Nothing.
        }
    }
}
