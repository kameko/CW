
namespace Caesura.CW.Platforms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.IO;
    
    /// <summary>
    /// This is specific to Windows and may not translate directly
    /// to other window managers. Translate as needed.
    /// </summary>
    public enum WallpaperStyle : int
    {
        Center	= 0,
        Tile	= 1,
        Stretch	= 2,
        Fit	    = 3,
        Fill	= 4,
        Span	= 5,
    }
    
    public interface IWallpaperManagerAbstraction : IDisposable
    {
        void RunOnce();
        void SetTempDirectory(string path);
        void SetTempDirectory(DirectoryInfo dir);
        bool SetWallpaper(Stream image_stream, WallpaperStyle style);
    }
}
