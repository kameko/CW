
namespace Caesura.CW.Platforms
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.InteropServices;
    
    public class PlatformAbstractionLayer : IPlatformAbstractionLayer
    {
        private IPlatformProvider provider;
        
        public OSPlatform Platform => provider.Platform;
        public IWallpaperManagerAbstraction WallpaperManager => provider.WallpaperManager;
        public DirectoryInfo DataPath => provider.DataPath;
        
        public PlatformAbstractionLayer()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                provider = new Windows.WindowsPlatformProvider();
            }
            else
            {
                provider = new UnsupportedPlatformProvider();
            }
        }
        
        public void OnStartup() => provider.OnStartup();
        
        public void OnShutdown() => provider.OnShutdown();
        
        public bool ChangeDataPath(DirectoryInfo dir) => provider.ChangeDataPath(dir);
        
        public bool ChangeDataPath(string path) => provider.ChangeDataPath(path);
        
        public bool IsValidPlatform() => provider.IsValidPlatform();
        
        public void RunAtStartup(bool run_at_startup = false) => provider.RunAtStartup(run_at_startup);
        
        public bool IsRunningAtStartup() => provider.IsRunningAtStartup();
        
        public void RunOnce() => WallpaperManager.RunOnce();
        
        public void SetTempDirectory(string path) => WallpaperManager.SetTempDirectory(path);
        
        public void SetTempDirectory(DirectoryInfo dir) => WallpaperManager.SetTempDirectory(dir);
        
        public bool SetWallpaper(Stream image_stream, WallpaperStyle style)
        {
            if (!IsValidPlatform())
            {
                throw new PlatformNotSupportedException();
            }
            
            return WallpaperManager.SetWallpaper(image_stream, style);
        }
        
        public bool SetWallpaper(FileInfo file, WallpaperStyle style)
        {
            if (!file.Exists)
            {
                throw new IOException($"File \"{file.FullName}\" does not exist.");
            }
            
            using var stream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read);
            return SetWallpaper(stream, style);
        }
        
        public bool SetWallpaper(string file, WallpaperStyle style) => SetWallpaper(new FileInfo(file), style);
        
        public void Dispose()
        {
            WallpaperManager.Dispose();
            provider.Dispose();
        }
    }
}
