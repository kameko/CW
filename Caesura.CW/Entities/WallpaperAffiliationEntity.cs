
namespace Caesura.CW.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    public class WallpaperAffiliationEntity
    {
        public bool Enabled { get; set; }
        public bool ReadOnly { get; set; }
        public Dictionary<string, int> Affiliations { get; set; }
        
        public WallpaperAffiliationEntity()
        {
            Affiliations = new Dictionary<string, int>();
        }
    }
}
