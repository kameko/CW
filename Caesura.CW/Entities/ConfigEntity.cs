
namespace Caesura.CW.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    public class ConfigEntity
    {
        public Dictionary<string, string> Folders { get; set; }
        public WebsitesConfigEntity Websites { get; set; }
        
        public ConfigEntity()
        {
            Folders  = new Dictionary<string, string>();
            Websites = new WebsitesConfigEntity();
        }
    }
}
