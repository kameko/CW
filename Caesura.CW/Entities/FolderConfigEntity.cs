
namespace Caesura.CW.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    public class FolderConfigEntity
    {
        public bool Enabled { get; set; }
        public string TimingLuaScriptPath { get; set; }
        
        public FolderConfigEntity()
        {
            TimingLuaScriptPath = string.Empty;
        }
    }
}
