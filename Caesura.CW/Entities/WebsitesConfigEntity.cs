
namespace Caesura.CW.Entities
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    
    public class WebsitesConfigEntity
    {
        public bool Enabled { get; set; }
        public string TimingLuaScriptPath { get; set; }
        
        public WebsitesConfigEntity()
        {
            TimingLuaScriptPath = string.Empty;
        }
    }
}
