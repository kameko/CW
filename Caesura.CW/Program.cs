﻿
namespace Caesura.CW
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Platforms;
    
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            using var pal = new PlatformAbstractionLayer();
            pal.OnStartup();
        }
    }
}
