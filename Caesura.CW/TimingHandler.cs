
namespace Caesura.CW
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Text;
    using NLua;
    
    // https://github.com/NLua/NLua 
    // interpreter.DoString(string) -> object (cast as needed).
    // interpreter["variable"]
    // LuaFunction is the type of a function. Returns multiple values,
    // so if you only need one, index the 0th element of the result list.
    // Example:
    //   var scriptFunc = interpreter["ScriptFunc"] as LuaFunction;
	//   var res = (int)scriptFunc.Call(3, 5).First();
    // To create object you only need to use the class name with the `()` (`MyClass()`).
    // To call instance methods, you need the `:` syntax (`instance:MyMethod()`).
    // Properties use the `.` syntax.
    // All methods, events or property need to be public available,
    // NLua will fail to call non-public members.
    // Sandboxing: `import = function () end` etc..
    
    public class TimingHandler : ITimingHandler
    {
        private Lua interpreter;
        
        public TimingHandler()
        {
            interpreter = new Lua();
            interpreter.State.Encoding = Encoding.UTF8;
            interpreter.LoadCLRPackage();
            // interpreter.DoString(@"
            // import ('MyAssembly', 'MyNamespace')
            // import ('System.Web') ");
        }
        
        public void Dispose()
        {
            interpreter.Dispose();
        }
    }
}
