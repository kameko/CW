
Need a way to tell the system which folders to use at what times. Allow the user to add an optional "cw.conf"
file which can further specify things like how much a user likes one wallpaper over others, so the system will
select that wallpaper. Also allow the system to automatically make these assumptions and update the cw.conf file
if the user often chooses a "next wallpaper" option for a specific wallpaper. Also store this weight in the cw.conf.
If cw.conf is locked, keep the information in memory and continuously attempt to write it until it unlocks.

We'll also need remote wallpaper websites. We can do this by local configs. Make a "Websites" config option in the main
config, which points to a folder full of configs for each website, or more specifically a URL endpoint (so one website
can provide multiple URLs, maybe one for daytime and one for nighttime.) The config file names don't matter, but the
user can call them things like "wallpapers.com.conf". The actual URL endpoint is inside the config, along with all
the settings, like when it should use these files, etc.

For conflicting configs, for example, a website and a folder both are set to use wallpapers at the same time, the
config can choose to either use both, prefer folder, or prefer website. If two folders or websites are conflicting,
the config can specify to combine them, or use the one with the config edited most recently.

If using a web config, have an option to save a copy of each wallpaper used into a folder.

Save a copy of the original wallpaper and have an option to set it back on graceful program shutdown.

Have some option to save the clipboard as a wallpaper. By default they go in the AppData "clipboard_content" folder,
but there should be options for which folder/collection to save it in as well.

Fade the wallpaper during transition (also see if we even need to do such a thing or if Windows does).
https://www.codeproject.com/articles/856020/draw-behind-desktop-icons-in-windows-plus 

Figure out how we're going to do the startup thing.
https://stackoverflow.com/questions/4897655/create-a-shortcut-on-desktop/14632782 
https://stackoverflow.com/questions/5089601/how-to-run-a-c-sharp-application-at-windows-startup 
Oh wait, why don't we just put a PowerShell script in Startup instead of a LNK?

If we make a GUI to adjust timing, we can simply make it generate Lua code to put into the config files.

Make sure the "open file location" button is accurate, takes into account renaming a file.
